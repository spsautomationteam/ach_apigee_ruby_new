@intg
Feature: Get Batches Current
    As an API consumer
    I want to query batches details requests
    So that I know they have been processed

    @get-all-CurrentBatches
    Scenario: Used to retrieve itemized detail about the transactions in the current batch.
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches/current
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode
		
	@get-batchCurrent-byPageNo
    Scenario: query current batch by page no 
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches/current?pageNumber=1
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode
        And response body path should contain "pageNumber":1
        And response body path should contain "reference"
        And response body path should contain "billing"
        And response body path should contain "shipping"
        And response body path should contain "customer"
        And response body path should contain "paymentType"
        And response body path should contain "service":"ACH"

	@get-Batches-byPageSize
    Scenario: query batch by page size 
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches/current?pageSize=1
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain "pageSize":1
		And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode		
		
	@get-batchCurrent-bySortDirectionAsc
    Scenario: query current batch by ascending
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches/current?sortDirection=ascending
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode	

	@get-batchCurrent-bySortDirectionDesc
    Scenario: query current batch by descending order
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches/current?sortDirection=descending
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode			
		
		 