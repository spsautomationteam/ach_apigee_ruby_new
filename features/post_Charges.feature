@intg
Feature: Post_Charges
	As an API consumer
	I want to create charge requests
	So that I know they can be processed

    @post-Charges_Sale
    Scenario Outline: Post a Sale for different Transaction class and account type
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": <TxClass>, "amounts":{ "total": 1.0 }, "account":{ "type": <accountType>, "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
	#	When I use HMAC and POST to /charges?type=Sale
		When I use HMAC and POST to /charges
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
        And response body path should contain "message":"ACCEPTED                        "
	Examples:
	|TxClass|accountType|
	|"ARC"|"Savings"|
	|"ARC"|"Checking"|
	|"CCD"|"Savings"|
	|"CCD"|"Checking"|
	|"PPD"|"Savings"|
	|"PPD"|"Checking"|
	|"RCK"|"Savings"|
	|"RCK"|"Checking"|
	|"TEL"|"Savings"|
	|"TEL"|"Checking"|
	|"WEB"|"Savings"|
	|"WEB"|"Checking"|
	
	@post-Charges_byAllTags-Sale
    Scenario: Post a Sale with all tags data passed [Except Prior Reference Tag]
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
        When I use HMAC and POST to /tokens
        Then response code should be 200
        And response body path should contain "status": 1
        And response body path should contain "message": "SUCCESS"
		And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"token_num",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
		When I use HMAC and POST to /charges/
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
	#	And response body path message should be ACCEPTED
        And response body path should contain "message":"ACCEPTED                        "
    #   And response body path should contain "status": 1
    #   And response body path should contain "message": "SUCCESS"
				
	@post-Charges_Sale-withVaultCreate
    Scenario: Post a Sale with vault operation as Create
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {  "operation": "Create"     }}
		When I use HMAC and POST to /charges
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
		And response body path should contain "message":"ACCEPTED                        "
		And response body path vaultResponse.message should be Success

  @post-Charges_Sale-withVaultRead
  Scenario: Post a Sale with vault operation as Read
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
    When I use HMAC and POST to /tokens
    Then response code should be 200
    And response body path should contain "status": 1
    And response body path should contain "message": "SUCCESS"
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": { "token": "token_num", "operation": "Read" }}
    When I use HMAC then POST to Vault Read /charges/
    Then response code should be 201
    And response header Content-Type should be application/json
    And response body path should contain "status":"Approved"
    And response body path should contain "message":"ACCEPTED                        "
    And response body path should contain "reference"
    And response body path should contain "orderNumber"

  @post-Charges_Sale-withVaultUpdate
  Scenario: Post a Sale with vault operation as Update
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
    When I use HMAC and POST to /tokens
    Then response code should be 200
    And response body path should contain "status": 1
    And response body path should contain "message": "SUCCESS"
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {         "token": "token_num",         "operation": "Update"     }}
    When I use HMAC then POST to Vault Read /charges/
    Then response code should be 201
    And response header Content-Type should be application/json
    And response body path should contain "status":"Approved"
    And response body path should contain "message":"ACCEPTED                        "
    And response body path should contain "reference"
    And response body path should contain "orderNumber"
    And response body path should contain "vaultResponse":{"status":1,"message":"Success"}
		
		
		#################### Negative Scenarios ###########################

	@post-Charges_ACH-Auth
    Scenario: Post an authorization request
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"isRecurring": true,"recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" } }
		When I use HMAC and POST to /charges?type=Authorization
        Then response code should be 401
        And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.	
	
	@post-Charges_ACH-Force
    Scenario: Post a force request
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges?type=Force
        Then response code should be 401
        And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.

  @post-Charges_ACH-Credit
  Scenario: Post a Credit request
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /charges?type=Credit
    Then response code should be 401
    And response body path code should be 000000
    And response body path message should be Internal Server Error
    And response body path detail should be Please contact support for assistance.
		
	@post-Charges_Sale-withoutTxClassTag
    Scenario: Post a Sale without transactionClass tag
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to {  "amounts":{"total": 1.0  }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 400
		And response body path detail should be InvalidRequestData : request: SecCode is required
		
	@post-Charges_Sale-empty-trxClass
    Scenario: Post a Sale with empty transactionClass
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path detail should be InvalidRequestData : request: SecCode is required

    @post-Charges_Sale-invalid-trxClass
    Scenario: Post a Sale with invalid transactionClass
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "ABC", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain Error converting value
		
	@post-Charges_Sale-negative-Amount
    Scenario: Post a Sale for invalidAmount
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{"total": -31.0  }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 400
		And response body path detail should be InvalidRequestData : request.Amounts: Invalid Total Amount
		
	 @post-Charges_Sale-invalid-Amount
    Scenario: Post a Sale with invalid amount
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
         And I set nonce header to nonce
         And I set timestamp header to timestamp
         And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": "ABC" }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain "InvalidRequestData : request: Total is required; Could not convert string to decimal: ABC. Path 'amounts.total', line 1, position 50."
		
	@post-Charges_Sale-missing-Amounts
    Scenario: Post a Sale with missing amounts
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain The Amounts field is required.
				
	@post-Charges_Sale-without-totalAmountVal
    Scenario: Post a Sale without total amount value
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{  }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 400
		And response body path detail should be InvalidRequestData : request: Total is required		
	
	@post-Charges_Sale-empty-Account
    Scenario: Post a Sale with empty account
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{  }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain The Type field is required.
        And response body path should contain The RoutingNumber field is required.
        And response body path should contain The AccountNumber field is required.
		
	@post-Charges_Sale-invalidAccountNo
    Scenario: Post a Sale for invalid AccountNumber
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "123" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 401
		And response body path detail should be You are a using certification environment which is restricted to test data only.
		
	@post-Charges_Sale-withoutAccountNo
    Scenario: Post a Sale without AccountNumber
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 400
		And response body path detail should be InvalidRequestData : request.Account: The AccountNumber field is required.
		
	@post-Charges_Sale-invalidRoutingNo
    Scenario: Post a Sale for invalid Routing Number
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "123", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 401
		And response body path detail should be You are a using certification environment which is restricted to test data only.
		
	@post-Charges_Sale-withoutRoutingNo
    Scenario: Post a Sale without RoutingNumber
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking","accountNumber": "12345678901234"  }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 400
		And response body path detail should be InvalidRequestData : request.Account: The RoutingNumber field is required.

  @post-Charges_Sale-WithoutCustomerData_WithTxClass
  Scenario Outline: Post a Sale for CCD and WEB tx class
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": <TxClass>, "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /charges
    Then response code should be 400
    And response body path should contain "code": "400000"
    And response body path should contain "message": "There was a problem with the request. Please see 'detail' for more."
    And response body path should contain "detail": "InvalidRequestData : Customer is required"

    Examples:
      |TxClass|
      |"CCD"|
      |"WEB"|

  @post-Charges-Sale-byInvalidEmailId
  Scenario: Post a Sale for invalid EmailId
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "ARC", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "testgmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /charges
    Then response code should be 400
    And response body path code should be 400000
    And response body path message should be There was a problem with the request. Please see 'detail' for more.
    And response body path should contain "detail": "InvalidRequestData : request.Customer: The Email field is not a valid e-mail address."

  @post-Charges-Sale-byBlankEmailId
    Scenario: Post a Sale for Blank EmailId
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "ARC", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.Customer: The Email field is not a valid e-mail address.
        And response body path should contain "detail": "InvalidRequestData : request.Customer: The Email field is not a valid e-mail address."
		
	@post-Charges_Sale-withoutBillingData
    Scenario: Post a Sale for invalidAccountNumber
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }}
		When I use HMAC and POST to /charges
        Then response code should be 400
        And response body path should contain "detail": "InvalidRequestData : request: The Billing field is required."
		
	@post-Charges_Sale-withoutAccountType
    Scenario: Post a Sale for invalidAccountNumber
		Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 400
        And response body path should contain "detail": "InvalidRequestData : request.Account: The Type field is required."

	@post-Charges_Sale-empty-Billing
    Scenario: Post a Sale with empty billing
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": {  } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain "detail": "InvalidRequestData : request.Billing: The Name field is required."

    @post-Charges_Sale-empty-Name
    Scenario: Post a Sale with empty name
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": {} } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain The First field is required
        And response body path should contain The Last field is required

    @post-Charges_Sale-empty-Names
    Scenario: Post a Sale with empty names
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "First": "", "Last": "" } } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain The First field is required
        And response body path should contain The Last field is required

    @post-Charges_Sale-missing-Addresses
    Scenario: Post a Sale with missing address data
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "First": "foo", "Last": "bar" } } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain Address is required
        And response body path should contain City is required
        And response body path should contain State is required
        And response body path should contain PostalCode is required

    @post-Charges_Sale-empty-Addresses
    Scenario: Post a Sale with empty address data
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "foo", "last": "bar" }, "address": "", "city": "", "state": "", "postalCode": "" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path code should be 400000
        And response body path should contain Address is required
        And response body path should contain City is required
        And response body path should contain State is required
        And response body path should contain PostalCode is required
