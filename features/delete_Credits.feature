@intg
Feature: Delete Credits 
	As an API consumer
	I want to delete/void credit requests based on the reference
		
	@delete-Credits_byCreditTxReference
    Scenario: delete credit from reference of transaction
      Given I set clientId header to clientId
      And I set merchantId header to merchantId
      And I set merchantKey header to merchantKey
      And I set content-type header to application/json
      And I set nonce header to nonce
      And I set timestamp header to timestamp
      And I set Authorization header to Authorization
      And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
      When I use HMAC and POST to /credits
      Then response code should be 201
      When I use HMAC and DELETE to /credits/
      Then response code should be 200
		

	################### Negative Scenarios ########################
	
	@delete-Credits_byAlreadyDeleted-CreditTx
    Scenario: delete charge from reference of Sale,Auth and force tranaction
      Given I set clientId header to clientId
      And I set merchantId header to merchantId
      And I set merchantKey header to merchantKey
      And I set content-type header to application/json
      And I set nonce header to nonce
      And I set timestamp header to timestamp
      And I set Authorization header to Authorization
      And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
      When I use HMAC and POST to /credits
      Then response code should be 201
      When I use HMAC and DELETE to /credits/
      Then response code should be 200
      When I use HMAC and DELETE to /credits/
      Then response code should be 404
   	  And response body path message should be Internal Server Error
	
	@delete-Credits_byInvalidReference
    Scenario: Try to delete credit by entering wrong reference
      Given I set clientId header to clientId
      And I set merchantId header to merchantId
      And I set merchantKey header to merchantKey
      And I set content-type header to application/json
      And I set nonce header to nonce
      And I set timestamp header to timestamp
      And I set Authorization header to Authorization
      When I use HMAC and DELETE to /credits/Test123
      Then response code should be 404
      And response body path code should be 000000
      And response body path message should be Internal Server Error
      And response body path detail should be Please contact support for assistance.

    #try to delete a settled transaction
	
	
		