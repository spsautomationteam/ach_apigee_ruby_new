class Ach

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"
# require_relative '../../support/API-tools'
  require_relative '../../support/hmac-tools'
  require 'date'


#----- Declaring Global variables ----------
  @reference_num = '0'
  @@content_type =''
  @authorization=''
  @auth=''
  @response=''
  @parsed_response=''

  @domain=''
  @basepath=''
  @clientId=''
  @clientSecret=''
  @merchantId=''
  @merchantKey=''

# ------------ constructor,Reading data from yaml file  ----------
  def initialize()
    @hmac = HmacTools.new()

    @header_map={}

    config = YAML.load_file('config.yaml')
    base_url = config['domain']
    @auth = config['authorization']
    @domain=config['domain']
    @basepath=config['basepath']
    @clientId=config['clientId']
    @clientSecret=config['clientSecret']
    @merchantId=config['merchantId']
    @merchantKey=config['merchantKey']

    puts "@base_url :#{base_url}"
    set_url base_url
  end

# -------- Set Authorization for json request  ----------
  def set_authorization(auth)
    @authorization =auth
  end

# -------- Set URL for json request  ----------
  def set_url(url)
    @url = url
  end

# -------- Set Headers for json request  ----------
  def set_header(header, value)
    puts "header : #{header}, value : #{value}"
    case header
      when "content-type"
        @header_map[header] = value
      when "Authorization"
        @header_map[header] = value
      when "clientId"
        if (value.eql? 'clientId')
          @header_map[header] = @clientId
        else
          @header_map[header] = value
        end
      when "clientSecret"
        if (header.eql? 'clientSecret')
          @header_map[header] = @clientSecret
        else
          @header_map[header] = value
        end
      when "merchantId"
        if (header.eql? 'merchantId')
          @header_map[header] = @merchantId
        else
          @header_map[header] = value
        end
      when "merchantKey"
        if (header.eql? 'merchantKey')
          @header_map[header] = @merchantKey
        else
          @header_map[header] = value
        end
    when "nonce"
    if (header.eql? 'nonce')
      @nonce = DateTime.now.strftime('%Q')
      @header_map['nonce'] = @nonce
    else
      @header_map[header] = value
    end
      when "timestamp"
        if (header.eql? 'timestamp')
          @timestamp = DateTime.now.strftime('%Q').to_i/1000
          @header_map['timestamp'] = @timestamp
        else
          @header_map[header] = value
        end

  end

   # puts "@header_map:#{@header_map}"

  end

# -------- Set Json Body  ----------
  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

# -------- Post HMAC  ----------
  def postHmac(path)
    post_ref path, ''
  end

# -------- Post existing Reference number  ----------
  def post_existing_refnum(path)
    post_ref path, @reference_num
  end

  def get_ping(path)

    full_url= @url+path

    puts "@domain#{@domain}"
    puts "full_url: #{full_url}"
#   puts "@@content_type #{@content-type}"
    puts "@@clientId #{@clientId}"
    puts "@@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@@merchantKey #{@merchantKey}"
    puts "@@nonce #{@nonce}"
    puts "@@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, @header_map['nonce'], @header_map['timestamp'])

  # puts "authorization #{@authorization}"

    if (!@header_map.has_key?'Authorization')
          puts "Authorization Missing"
        else
          if (!@header_map['Authorization'].eql? 'invalid-hmac')
            @header_map['Authorization'] = @authorization
        end
    end

    puts "ping @header_map #{@header_map}"

#Do post action and get response
    begin

    @response = RestClient.get full_url, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

#####################################################


# -------- Post transaction using Reference number  ----------
  def post_ref(path, ref_num)

    if path == '/charges/'
    # puts "put body #{@json_body}"
      full_url= @url+'/charges'
      myjson_body= @json_body
      myjson_body["token_num"]= @token_num
      @json_body = myjson_body

    else
      if path == '/credits/'
      # puts "put body #{@json_body}"
        full_url= @url+'/credits'
        myjson_body= @json_body
        myjson_body["token_num"]= @token_num
        @json_body = myjson_body

      else
        if path == '/credits/{reference}'
      #   puts "Post Credit Reference Body #{@json_body}"
          full_url= @url+'/credits/'+@reference_num

        else
          if path == '/credits/Test123'
       #    puts "Post Credit Reference Body #{@json_body}"
            full_url= @url+'/credits/Test123'

          else
            full_url= @url+path
          end
        end
      end
    end

    if (!ref_num.empty?)
      full_url +='/'+ref_num
    end

    #Debug urls
    puts "path#{path}"
    puts "@domain#{@domain}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"

    @authorization = @hmac.hmac(@clientSecret, 'POST', full_url, @json_body, @merchantId, @header_map['nonce'], @header_map['timestamp'])

    @header_map['Authorization'] = @authorization

    #Do post action and get response
    begin

      if path == '/tokens'

        @response = RestClient.post full_url, @json_body, @header_map
        @token_num = JSON.parse(@response.body)['vaultResponse']['data']
        puts "@token_num: #{@token_num}"

      else
        @response = RestClient.post full_url, @json_body, @header_map
        @reference_num = JSON.parse(@response.body)['reference']
        @orderNumber = JSON.parse(@response.body)['orderNumber']
        @accountNumber = JSON.parse(@response.body)['accountNumber']
        @total = JSON.parse(@response.body)['total']

        puts "@orderNumber #{@orderNumber}"
        puts "@reference_num: #{@reference_num}"
        puts "@total: #{@total}"

      end

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end

# -------- Used to process a Credit and Batches transactions using Reference number  ----------
  def post_credits_reference(body)

    if body == '/credits/Test123'
      full_url= @url+'/credits/Test123'
      json_body = '{"transactionId": "",     "deviceId": "7894560",     "amount": 20,     "terminalNumber": "" }'
    else
      if body == '/batches/current'
        full_url= @url+'/batches/current'
        json_body = '{"settlementType": "Ach"}'
      else
        full_url= @url+'/credits/'+@reference_num
        json_body = body
      end
    end

    #Debug urls
    puts "@domain#{@domain}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{json_body}"


    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'POST', full_url, json_body, @merchantId, @header_map['nonce'], @header_map['timestamp'])

    # puts "hmac #{@authorization}"

    #Do post action and get response
    begin

      @response = RestClient.post full_url, @json_body, @header_map

      @reference_num = JSON.parse(@response.body)['reference']
      @orderNumber = JSON.parse(@response.body)['orderNumber']
      @accountNumber = JSON.parse(@response.body)['accountNumber']
      puts "@orderNumber #{@orderNumber}"
      @total = JSON.parse(@response.body)['total']

      puts "@reference_num: #{@reference_num}"
        # puts "response>>> #{@response}"
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end

# ---------- Get the list of existing transaction details ------------------
  def get_listof_transactions(type)

    full_url= @url+'/charges?type='+type

    #Debug urls
    puts "type#{type}"
    puts "@domain#{@domain}"
    url = @domain +'/charges?type='+ type
    puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', url, '', @merchantId, @header_map['nonce'], @header_map['timestamp'])

    # puts "authorization #{@authorization}"

    #Do post action and get response
    begin

      @response = RestClient.get full_url, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end

# -------------- Get the current transaction details -------------
  def get_current_transdetails()

    full_url= @url+'/charges?reference='+@reference_num

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    puts "@get_full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, @header_map['nonce'], @header_map['timestamp'])

   # puts "authorization #{@authorization}"

    #Do post action and get response
    begin

      @response = RestClient.get full_url, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

# ----------- Get current Credit transaction details ----------------------

  def get_current_credit_transdetails(path)

    if path == '/credits/'
      full_url= @url+'/credits/'+@reference_num
    else
      full_url= @url+'/credits?reference='+@reference_num
    end

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    puts "@get_full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, @header_map['nonce'], @header_map['timestamp'])

    # puts "authorization #{@authorization}"

    #Do post action and get response
    begin

      @response = RestClient.get full_url, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

# ------------ Get current transaction details by reference number -----------

  def get_current_transdetails_by_refnumber(method)

    if method == '/charges/{reference}/lineitems'
      full_url= @url+'/charges/'+@reference_num+'/lineitems'
    else
      if method == '/charges/Test123/lineitems'
        full_url= @url+'/charges/Test123/lineitems'
      else
        full_url= @url+method+@reference_num
      end
    end

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    puts "@get_full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

  # @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, @header_map['nonce'], @header_map['timestamp'])
    @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, @nonce, @timestamp)

   #  puts "authorization #{@authorization}"

    #Do post action and get response
    begin

    # @response = RestClient.get full_url, @header_map
      @response = RestClient.get full_url, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => 'application/json', :nonce => @nonce, :timestamp => @timestamp

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

# ----------- Get current Credit transaction details by reference number -------------

  def get_current_credit_transdetails_by_refnumber()
    full_url= @url+'/credits/'+@reference_num

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    url = @domain +'/credits/'+ @reference_num
    puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', url, '', @merchantId, @header_map['nonce'], @header_map['timestamp'])

    # puts "authorization #{@authorization}"

    #Do post action and get response
    begin

      @response = RestClient.get full_url, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

# ------------ Used to update the card data associated with a vault token ------------------

  def put_transaction(val)

    if val == '/tokens/'
      puts "put_token_num#{@token_num}"
      full_url= @url+'/tokens/'+@token_num
      put_body = @json_body
      puts "put body #{put_body}"

    else
      if val == '/tokens/Test123'
        full_url= @url+'/tokens/Test123'
        put_body = @json_body
        puts "put body #{put_body}"

      else
        puts "put_reference_num#{@reference_num}"
        full_url= @url+'/charges/'+@reference_num
        # amount = amount.to_i
        put_body = '{ "amounts": { "tip": 4.24, "total": '+val+',  "tax": 2.12, "shipping": 1.06 } }'
        puts "put body #{put_body}"

      end
    end

    puts "@domain#{@domain}"
    puts "@put_full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'PUT', full_url, put_body, @merchantId, @header_map['nonce'], @header_map['timestamp'])
  #  @authorization = @hmac.hmac(@clientSecret, 'PUT', full_url, put_body, @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"

    puts "@header_map #{@header_map}"

    #Do post action and get response
    begin

#      @response = RestClient.put full_url, put_body, @header_map
     @response = RestClient.put full_url, put_body, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => 'application/json', :nonce => @nonce, :timestamp => @timestamp

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

#  ----------- Capture Authorization by Invalid Reference number -----------

  def captureAuthorization_byInvalid_refrencenumber(path, amount)

    full_url= @url+path

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    url = @domain +path
    puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    put_body = '{ "amounts": { "tip": 4.24, "total": '+amount+',  "tax": 2.12, "shipping": 1.06 } }'

    puts "capture body #{put_body}"

    @authorization = @hmac.hmac(@clientSecret, 'PUT', url, put_body, @merchantId, @header_map['nonce'], @header_map['timestamp'])

   # puts "authorization #{@authorization}"

    #Do post action and get response
    begin

      @response = RestClient.put full_url, put_body, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

#  --------------- GET the transaction details using parameter value --------------

  def get_by_paramvalue(path)

    if path == '/batches'
      full_url= @url+path
    else
      if path == '/batches/current'
        full_url= @url+path
      else
        if path == '/batches/'
          full_url= @url+path+@reference_num
        else
          if path == '/batches/{reference}?pageSize=2'
            full_url= @url+'/batches/'+@reference_num+'?pageSize=2'
          else
            if path == '/batches/{reference}?pageNumber=1'
              full_url= @url+'/batches/'+@reference_num+'?pageNumber=1'
            else
              if path == '/batches/{reference}?sortsortField=Ascending&sortField=date'
                full_url= @url+'/batches/'+@reference_num+'?sortsortField=Ascending&sortField=date'
              else
                if path == '/batches/{reference}?sortsortField=Descending&sortField=date'
                  full_url= @url+'/batches/'+@reference_num+'?sortsortField=Descending&sortField=date'
                else
                  if path == '/batches/Test123'
                    full_url= @url+'/batches/Test123'
                  else
                    if path == '/transactions?reference='
                      full_url= @url+path+@reference_num
                    else
                      if path == '/transactions?orderNumber='
                        full_url= @url+path+@orderNumber
                      else
                        if path == '/charges?reference='
                          full_url= @url+path+@reference_num
                        else
                          if path == '/credits?reference='
                            full_url= @url+path+@reference_num
                          else
                            if path == '/charges?orderNumber='
                              full_url= @url+path+@orderNumber
                            else
                              full_url= @url+path
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end

    # puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    puts "full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, @header_map['nonce'], @header_map['timestamp'])

    # puts "authorization #{@authorization}"

    @header_map['Authorization'] = @authorization

    #Do post action and get response
    begin

      @response = RestClient.get full_url, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

# ---------------- Delete the required transaction using Reference number ----------

  def delete_transaction_byreference(path)

    if path == '/tokens'
      full_url= @url+'/tokens/'+@token_num
    else
      if path == '/charges/'
        full_url= @url+'/charges/'+@reference_num
      else
        if path == '/credits/'
          full_url= @url+'/credits/'+@reference_num
        else
          if path == '/charges/{reference}/lineitems'
            full_url= @url+'/charges/'+@reference_num+'/lineitems'
          else
            if path == '/charges/Test123/lineitems'
              full_url= @url+'/charges/Test123/lineitems'
            else
              full_url= @url+path
            end
          end
        end
      end
    end

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    puts "@get_full_url: #{full_url}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{@nonce}"
    puts "@timestamp #{@timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'DELETE', full_url, '', @merchantId, @header_map['nonce'], @header_map['timestamp'])

    @header_map['Authorization'] = @authorization

    #Do post action and get response
    begin

      @response = RestClient.delete full_url, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

# ----------- Verify parameter value into the response body ------------------

  def verify_response_params_contain(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"
    if @parsed_response[response_param].nil?
      expect(@parsed_response[response_param].include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
    else
      expect(@parsed_response[response_param].to_s.delete('').include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
    end
  end

# ----------- Verify Token number into the response body ------------------

  def verify_response_for_tokennumber()
    if !@response.nil?
      expect(@response.include? @token_num).to be_truthy, "Expected : #{@token_num} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{@token_num} ,got : Empty response}"
    end
  end

# ----------- Verify response value into the response body ------------------

  def verify_response_value_contain(response_value)
    puts "response_value:#{response_value}"
    if !@response.nil?
      expect(@response.include? response_value).to be_truthy, "Expected : #{response_value} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{response_value} ,got : Empty response}"
    end
  end

# ----------- Verify response value using reqular expressions into the response body ------------------

  def verify_regex(val, exp)
    if (exp.include? '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
      regex = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/
    else
      if (exp.include? '\d{1,2}')
        regex = /^\d{1,3}$/
      end
    end
    puts "val#{val}"
    resp_val =@parsed_response[val]
    puts "resp_val#{resp_val}"

    status = resp_val.match?(regex)
    puts "status #{status}"

    expect(status).to be_truthy, "#{val} validation failed"

  end

# ----------- Verify Reference number into the response body ------------------

  def verify_response_contain_refnumber()
    puts "@reference_num #{@reference_num}"
    if !@response.nil?
      expect(@response.include? @reference_num).to be_truthy, "Expected : #{@reference_num} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{@reference_num} ,got : Empty response}"
    end
  end

# ----------- Verify Order number into the response body ------------------

  def verify_response_contain_ordernumber()
    puts "@reference_num #{@orderNumber}"
    if !@response.nil?
      expect(@response.include? @orderNumber).to be_truthy, "Expected : #{@orderNumber} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{@orderNumber} ,got : Empty response}"
    end
  end

# ----------- Verify response body should not contain othe info ------------------

  def verify_response_value_not_contain(response_value)
    puts "response_value:#{response_value}"
    if !@response.nil?
      expect(@response.include? response_value).to be_falsey, "Expected : #{response_value} ,got : #{@response} "
    else
      expect(true).to be_falsey, "Expected : #{response_value} ,got : Empty response}"
    end
  end

# ----------- Verify response body Empty ------------------
  def verify_response_empty
    expect(@response.empty?).to be_truthy, "Expected : Response  empty ,got : #{@response} \nResponesbody:#{@parsed_response}"
  end

# ----------- Verify response Code into response body ------------------

  def verify_response_code resp_code
    expect(@response.code.to_s).to eql(resp_code), "Expected : #{resp_code} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
  end

# ----------- Verify response parameters into response body ------------------

  def verify_response_params(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"

    if response_param.include? "."
      response_param1 = response_param.split('.').first
      response_param2 = response_param.split('.').last
      #puts "response_param1:#{response_param1} ,response_param2#{response_param2}"
      expected_val =@parsed_response[response_param1][response_param2]
      if expected_val.nil?
        expect(expected_val).to eql(value), "Expected : #{value} ,got : #{expected_val} \nResponesbody:#{@parsed_response}\n"
      else
        # puts expected_val.to_s.strip.length
        expect(expected_val.to_s.strip).to eql(value), "Expected : #{value} ,got : #{expected_val.to_s.strip} \nResponesbody:#{@parsed_response}\n"
      end
    else

      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param]).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"

      else
        expect(@parsed_response[response_param].to_s.delete('')).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
      end

    end
  end

# ----------- Verify response headers into response body ------------------

  def verify_response_headers(header_param, value)
    puts "header_param:#{header_param} , value:#{value}"
    expect(@response.headers[:content_type]).to include(value), "Expected : #{value} ,got : #{@response.headers[:content_type]} \nResponesbody:#{@parsed_response}"
  end

end
