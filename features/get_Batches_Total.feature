@intg
Feature: Get Batches Total
    As an API consumer
    I want to query batches total
    So that I know they have been processed

    @get-Batches-Total
    Scenario: query all current batch Total
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches/totals
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain count
        And response body path should contain net
		And response body path should contain volume

  @get-Batches-Total_byStartDate
  Scenario: query all current batch total with start date
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /batches/totals?startDate=01-01-2017
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain count
    And response body path should contain net
    And response body path should contain volume

  @get-Batches-Total_byEndDate
  Scenario: query all current batch total with end date
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /batches/totals?endDate=01-01-2018
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain count
    And response body path should contain net
    And response body path should contain volume

  # +++++++++++++ Negative Scenarios +++++++++++++++++
		
	@get-Batches-Total_byInvalidStartDate
     Scenario: Verify the API Get Batches Total with Future StartDate
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /batches/totals?StartDate=2030-12-12
        Then response code should be 200
        And response body path should contain "count":0
        And response body path should contain "net":0
		And response body path should contain "volume":0	
		
	@get-Batches-Total_byInvalidEndDate
     Scenario: Verify the API Get Batches Total with Past EndDate
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /batches/totals?EndDate=2012-12-12
        Then response code should be 200
        And response body path should contain "count":0
        And response body path should contain "net":0
		And response body path should contain "volume":0	
		
		
