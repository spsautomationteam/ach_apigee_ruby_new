@intg
Feature: Get Batches
    As an API consumer
    I want to query batches details requests
    So that I know they have been processed

    @get-all-Batches
    Scenario: Used to query settled batches by date. Results include itemized details, such as settlement date and reference number.
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches
      # When I get all Batch Transaction Details
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain /ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode
	
	@get-Batches-byStartDate
    Scenario: query batch by Start Date
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches?startDate=2017-10-01
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain 2017-10-01

	@get-Batches-byEndDate
    Scenario: query batch by End Date 
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches?endDate=2017-12-30
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain 2017-12-30
		
	@get-Batches-byPageNo
    Scenario: query batch by page no 
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches?pageNumber=10
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode	
		
	@get-Batches-byPageSize
    Scenario: query batch by page size 
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches?pageSize=1
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain "pageSize":1
		And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode
		
	@get-Batches-bySortDirectionAsc
    Scenario: query batch by ascending order 
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches?sortDirection=ascending
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode	

	@get-Batches-bySortDirectionDesc
    Scenario: query batch by descending order 
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches?sortDirection=descending
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain transactionCode		


############  Negative Scenarios  #################		

	@get-Batches_byInvalidStartDate
     Scenario: Verify the API Get Batches with Future StartDate
           Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
		When I use HMAC and GET /batches?StartDate=2030-12-12
        Then response code should be 200
        And response body path should contain "startDate":"2030-12-12"
		And response body path should not contain ACH
		
	@get-Batches_byInvalidEndDate
     Scenario: Verify the API Get Batches with Past EndDate
           Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
		When I use HMAC and GET /batches?EndDate=2012-12-12
        Then response code should be 200
        And response body path should contain "endDate":"2012-12-12"	
		And response body path should not contain ACH		
		
		 