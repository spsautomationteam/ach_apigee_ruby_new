
# Set required Headers for Json request
And(/^I set (.*) header to (.*)$/) do |header, value|
  @ach.set_header header, value
end

# Set Json Body for request
And(/^I set body to (.*)$/) do |json|
  @ach.set_json_body json
end

# Create HMAC and Send POST request
When(/^I use HMAC and POST to (.*)$/) do |arg1|
   @ach.postHmac arg1
end

# Create HMAC and Send PUT request
When(/^I use HMAC and PUT to (.*)$/) do |text|
  @ach.put_transaction text
end

# Create HMAC and Send POST request with Vault Read
When(/^I use HMAC then POST to Vault Read (.*)$/) do |text|
  @ach.postHmac text
end

# Create HMAC and Send POST request with Reference number
When(/^I use HMAC then POST Credit Reference on the same (.*) transaction using (.*)$/) do |type, text|
  @ach.postHmac text
end

# Verify required 'response Headers' at API response
And(/^response header (.*) should be (.*)$/) do |header_param, val|
  @ach.verify_response_headers header_param, val
end

# Verify required Parameters at response Body
Then(/^response body path (.*) should be (.*)$/) do |param, val|
  @ach.verify_response_params param, val
end

# Verify required Parameters at response Body by applying regular expressions
Then(/^response body path (.*) must with regex (.*)$/) do |param, val|
  @ach.verify_regex param, val
end

# Verify required Parameters at response Body using contain method
Then(/^response body path (.*) should contain (.*)$/) do |param, val|
  @ach.verify_response_params_contain param, val
end

# Verify required Parameters at response Body using contain method
Then(/^response body path should contain (.*)$/) do | val |
  @ach.verify_response_value_contain  val
end

# Verify reference number at response Body using contain method
Then(/^response body should contain above performed transaction reference$/) do
  @ach.verify_response_contain_refnumber
end

# Verify Token number at response Body using contain method
Then(/^(.*) response body path should contain same token number$/) do |text|
  @ach.verify_response_for_tokennumber
end

# Verify Same reference number at response Body using contain method
Then(/^response body should contain the same reference number$/) do
  @ach.verify_response_contain_refnumber
end

# Verify Order number at response Body using contain method
Then(/^response body should contain the same order number$/) do
  @ach.verify_response_contain_ordernumber
end

# Verify response body should not contain required parameters
Then(/^response body path should not contain (.*)$/) do |val|
  @ach.verify_response_value_not_contain val
end

# Get list of existed transaction records
Then(/^I get a list of existing (.*) transactions$/) do |type|
  @ach.get_listof_transactions type
end

# Get that same transaction details
Then(/^I get that same transaction details$/) do
  @ach.get_current_transdetails
end

# Using HMAC and GET same credit transaction details
Then(/^I use HMAC then GET same credit transaction details using (.*)$/) do | path |
  @ach.get_current_credit_transdetails path
end

# Get that same transaction details using reference number
Then(/^I get that same transaction details using reference number$/) do
    @ach.get_current_transdetails
end

# Get same transaction details by using reference number and other parameters
Then(/^I get that same transaction details by using reference number and (.*)$/) do | method |
  @ach.get_current_transdetails_by_refnumber method
end

# Create HMAC and GET the details
Then(/^I use HMAC and GET (.*)$/) do | val |
  @ach.get_by_paramvalue val
end

# Create HMAC and GET Sage details
Then(/^I GET (.*)$/) do | val |
  @ach.get_ping val
end

# Create HMAC then GET List of Existing Settled Batches Transactions using parameters value
Then(/^I use HMAC then GET List of Existing Settled Batches Transactions using (.*)$/) do | path |
  @ach.get_by_paramvalue path
end

# Create HMAC then GET Batches References using parameters value
Then(/^I use HMAC then GET Batches References using (.*)$/) do | path |
  @ach.get_by_paramvalue path
end

# Create HMAC then GET List of Existing Current Settled Batches Transactions using parameters value
Then(/^I use HMAC then GET List of Existing Current Settled Batches Transactions using (.*)$/) do | path |
  @ach.get_by_paramvalue path
end

# Create HMAC and then GET a list of existing transaction details
Then(/^I use HMAC and then GET a list of existing (.*) transaction$/) do |type|
  @ach.get_listof_transactions type
end

# Create HMAC and Post data with reference
And(/^I post data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @ach.post_ref path, reference
end

# Create HMAC and Post data with existing ref num for transaction
And(/^I post data with existing ref num for transaction (.*)$/) do |path|
  @ach.post_existing_refnum path
end

# Verify response Code
Then(/^response code should be (.*)$/) do |responsecode|
  @ach.verify_response_code responsecode
end

# Verify whether Response Empty
Then(/^response is empty$/) do
  @ach.verify_response_empty
end

# Capture/PUT the Auth transaction with certain Amount
Then(/^I Capture the (.*) transaction with Total Amount (.*)$/) do |typeTrans, amount |
  # puts "Type of Transaction for Capture #{typeTrans}"
  @ach.put_transaction amount
end

# Capture/PUT the Auth transaction with certain Amount using Auth reference number
Then(/^Try to Capture the Transaction with (.*) Reference Number (.*) and Total Amount (.*)$/) do | text, path, amount |
  @ach.captureAuthorization_byInvalid_refrencenumber path, amount
end

# Create HMAC and DELETE the transaction
Then(/^I use HMAC and DELETE to (.*)$/) do | path |
  @ach.delete_transaction_byreference path
end

# Create HMAC and POST Credits Reference using Json Body
Then(/^I use POST Credits Reference using Body (.*)$/) do | body |
  @ach.post_credits_reference body
end

# Create HMAC and POST Credits Reference using Invalid reference number
Then(/^I use HMAC and POST Credits Reference using Invalid reference number (.*)$/) do | body |
  @ach.post_credits_reference body
end

# Create HMAC and Settled all current set of transactions
Then(/^I use HMAC and settle all current set of transactions using (.*)$/) do | body |
  @ach.post_credits_reference body
end

