@intg
Feature: Post Credits on previous perfromed Tx
	As an API consumer
	I want to post credit requests based on the previous transaction reference without knowing card number and expiration date
	So that I know they have been processed

    @post-credits-Reference_byCreditTxReference
    Scenario: post credit transaction from previous Credit transaction reference
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
        And I set body to {"amount": 10}
        When I use HMAC then POST Credit Reference on the same Credit transaction using /credits/{reference}
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
        And response body path should contain "message":"ACCEPTED                        "
        And response body path should contain "orderNumber"
        And response body path should contain "reference"
     
	@post-credits-Reference_byCreditTxReference-WithAllTag
    Scenario: post credit transaction from previous Credit transaction reference
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
        And I set body to {"amount": 1,"deviceId" : "Test"}
        When I use HMAC then POST Credit Reference on the same Credit transaction using /credits/{reference}
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
        And response body path should contain "message":"ACCEPTED                        "
        And response body path should contain "orderNumber"
        And response body path should contain "reference"

		
###################### Negative scenarios ###########################

	@post-credits-Reference_bySaleTxReference
    Scenario: post credit reference transaction by Sale transaction reference
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges?type=Sale
        Then response code should be 201
        And response body path status should be Approved
        And I set body to {"amount": 10}
        When I use HMAC then POST Credit Reference on the same Sale transaction using /credits/{reference}
        Then response code should be 404
        And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.

	@post-credits-Reference_withAmountValZero
    Scenario: post credit reference transaction by passing the amount as Zero
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
        And I set body to {"amount": 0}
        When I use HMAC then POST Credit Reference on the same Credit transaction using /credits/{reference}
        Then response code should be 400
        And response body path should contain "detail": "InvalidRequestData : INVALID T_AMT

    @post-credits-Reference_InvalidAmountVal
    Scenario: post credit reference transaction by passing invalid amount
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
        And I set body to {"amount": -1.90}
        When I use HMAC then POST Credit Reference on the same Credit transaction using /credits/{reference}
        Then response code should be 400
        And response body path should contain "detail": "InvalidRequestData : INVALID T_AMT

	@post-credits-Reference_NullBodyData
    Scenario: post credit reference transaction by passing null body data
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
        And I set body to {}
        When I use HMAC then POST Credit Reference on the same Credit transaction using /credits/{reference}
        Then response code should be 400
        And response body path should contain "detail": "InvalidRequestData : request: The Amount field is required."

	@post-credits-Reference_NullRequest
    Scenario: post credit reference transaction from previous transaction reference without passing json body data
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
        And I set body to '        '
        When I use HMAC then POST Credit Reference on the same Credit transaction using /credits/{reference}
        Then response code should be 400
        And response body path detail should be Invalid JSON request in message request, check syntax
		
	@post-credits-InvalidReference
    Scenario: post credit reference transaction from previous transaction reference by passing invalid reference
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
        And I set body to {"amount": 1}
        When I use HMAC then POST Credit Reference on the same Credit transaction using /credits/Test123
        Then response code should be 400
        And response body path should contain "code": "400000",
        And response body path should contain "message": "There was a problem with the request. Please see 'detail' for more.",
        And response body path should contain "detail": "InvalidRequestData : INVALID T_REFERENCE
		
	@post-credits-Reference_WithTransCode_NotAppliCable
    Scenario: post credit reference transaction by Transaction Code
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
     #  And I set body to {"amount": 1,"deviceId" : "Test"}   ==> Valid Json body with 2 parameters
        And I set body to {"amount": 1,"deviceId" : "Test","transactionCode": "7"}
        When I use HMAC then POST Credit Reference on the same Credit transaction using /credits/{reference}
        Then response code should be 400
		And response body path code should be 100007
		And response body path detail should be Invalid JSON request in message request, check syntax
        
				
                                
    