@intg
Feature: Delete_Charges 
	As an API consumer
	I want to delete/void charge requests based on the reference

  @delete-Charges_byReferenced-Sale
  Scenario: delete charge from reference of Sale tranaction
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
    When I use HMAC and POST to /charges
    Then response code should be 201
    And response body path should contain "status":"Approved"
    And response body path should contain "message":"ACCEPTED                        "
    And response body path should contain "reference"
    And response body path should contain "orderNumber"
    When I use HMAC and DELETE to /charges/
    Then response code should be 200
  
	
	################ Negative Scenarios #######################
	
#	****** Commented below code it's required clarification

  @delete-Charges_byReferenced-Credit
  Scenario: delete charge from reference of Credit tranaction
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And response body path status should be Approved
    When I use HMAC and DELETE to /charges/
    Then response code should be 404
    And response body path code should be 000000
    And response body path message should be Internal Server Error
    And response body path detail should be Please contact support for assistance.

	@delete-Charges_byReferenced-Force
    Scenario: delete charge from reference of Auth tranaction
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
        When I use HMAC and POST to /charges?type=Force
        Then response code should be 201
        And response body path status should be Approved
        When I use HMAC and DELETE to /charges/
        Then response code should be 404
        And response body path code should be 000000
        And response body path message should be Internal Server Error
        And response body path detail should be Please contact support for assistance.
		
	@delete-Charges_byReferenced-Auth
   Scenario: delete charge from reference of Auth tranaction
      Given I set clientId header to clientId
      And I set merchantId header to merchantId
      And I set merchantKey header to merchantKey
      And I set content-type header to application/json
      And I set nonce header to nonce
      And I set timestamp header to timestamp
      And I set Authorization header to Authorization
      And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
      When I use HMAC and POST to /charges?type=Authorization
      Then response code should be 201
      And response body path status should be Approved
      When I use HMAC and DELETE to /charges/
      Then response code should be 404
      And response body path code should be 000000
      And response body path message should be Internal Server Error
      And response body path detail should be Please contact support for assistance.

  @delete-Charges_byAlready-DeletedCharge
  Scenario: Try to delete already delated charge
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
    When I use HMAC and POST to /charges?type=Sale
    Then response code should be 201
    And response body path status should be Approved
    When I use HMAC and DELETE to /charges/
    Then response code should be 200
    When I use HMAC and DELETE to /charges/
    Then response code should be 404
    And response body path code should be 000000
    And response body path message should be Internal Server Error
    And response body path detail should be Please contact support for assistance.

  @delete-Charges_byinvalid_TxReference
    Scenario: Try to delete charge by entering wrong reference
      Given I set clientId header to clientId
      And I set merchantId header to merchantId
      And I set merchantKey header to merchantKey
      And I set content-type header to application/json
      And I set nonce header to nonce
      And I set timestamp header to timestamp
      And I set Authorization header to Authorization
      When I use HMAC and DELETE to /charges/Test123
      Then response code should be 404
      And response body path code should be 000000
      And response body path message should be Internal Server Error
      And response body path detail should be Please contact support for assistance.
