@intg
Feature: Get Batches Current Summary
    As an API consumer
    I want to query batches current summary
    So that I know they have been processed

    @get_batches_current_summary
    Scenario: Used to retrieve summarized information, such as count and volume, about the transactions in the current batch.
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        When I use HMAC and GET /batches/current/summary
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain paymentType
        And response body path should contain authTotal
		And response body path should contain saleCount
		And response body path should contain saleTotal
		And response body path should contain creditCount
		And response body path should contain creditTotal
		And response body path should contain totalCount
		And response body path should contain totalVolume


