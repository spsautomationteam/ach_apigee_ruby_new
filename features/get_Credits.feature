@intg
Feature: Get Credits
    As an API consumer
    I want to query credit requests
    So that I know they have been processed

  @get-All-CreditTransactions
  Scenario: query all credits
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    When I use HMAC and GET /credits
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain ACH
    And response body path should contain Credit
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "service":"ACH"
    And response body path should contain "type":"Credit"
    And response body path should not contain Bankcard
    And response body path should not contain "type":"Sale"
    And response body path should not contain transactionCode

	@get-Credits_byStartDate
    Scenario: query all credits by StartDate
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?StartDate=2017-07-30
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain /ACH
        And response body path should not contain Bankcard
        And response body path should not contain Sale
        And response body path should contain Credit
        And response body path should contain "startDate":"2017-07-30"
		
	@get-Credits_byEndDate
    Scenario: query all credits by End Date
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?EndDate=2017-07-18
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain /ACH
        And response body path should not contain Bankcard
        And response body path should not contain Sale
        And response body path should contain Credit
        And response body path should contain "endDate":"2017-07-18"		
                                
	@get-Credits_byPageNumber
    Scenario: query all credits by PageNumber
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?pageNumber=1
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain Sale
        And response body path should contain Credit
        And response body path should contain "pageNumber":1		
		
	@get-Credits_byPageSize
    Scenario: query all credits by Page size
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?pageSize=3
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain Sale
        And response body path should contain Credit
        And response body path should contain "pageSize":3		
		
	@get-Credits_bysortDescending-sortField
    Scenario: query all credits by Sorting set to descending
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?sortDirection=Descending&sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain Sale
        And response body path should contain Credit
        And response body path should not contain transactionCode
		
	@get-Credits_bysorAscending-sortField
    Scenario: query all credits by Sorting set to Ascending
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?sortDirection=Ascending&sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain /ACH
        And response body path should not contain Bankcard
        And response body path should not contain Sale
        And response body path should contain Credit
        And response body path should not contain transactionCode
		
	@get-Credits_byIsPurchaseCard
    Scenario: query all credits by ispurchase card status
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?isPurchaseCard=false
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
        And response body path should not contain Bankcard
        And response body path should not contain Sale
        And response body path should contain Credit
		And response body path should not contain "isPurchaseCard"=false
		
	@get-Credits_byAccountNumber
    Scenario: query all credits by account number
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?accountNumber=7890
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain ACH
		And response body path should contain Credit
        And response body path should not contain Sale       
        And response body path should contain "accountNumber":"XXXXXX7890"		
		
	@get-Credits_byOrderNumber
    Scenario: query all credits by order number
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 201
        And response body path status should be Approved
        When I use HMAC and GET /credits?orderNumber=
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain the same order number

  @get-Credits_byReference
  Scenario: query all credits by reference
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And response body path status should be Approved
    When I use HMAC and GET /credits?reference=
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body should contain the same reference number

  @get-Credits_byTotalAmount
  Scenario: query all credits by total amount
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 4.5 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And response body path status should be Approved
    When I use HMAC and GET /credits?totalAmount=4.5
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "total":4.5000
		
       
	###################### Negative scenarios ###########################

	@get-Credits_tryToGetSaleTransDetails
    Scenario: query to get Sale details from Credit API
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges?type=Sale
        Then response code should be 201
		When I use HMAC and GET /credits?reference=
        Then response code should be 200
        And response body path should contain "startDate":"2030-12-30"
        And response body path should not contain "reference"
        And response body path should not contain "service"
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

  @get-Credits-StartDate_AsFutureDate
    Scenario: Verify the API Get Credits with Future StartDate
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?StartDate=2030-12-30
        Then response code should be 200
        And response body path should contain "startDate":"2030-12-30"
		And response body path should not contain "reference"
		And response body path should not contain "service"
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

	@get-Credits-EndDate_AsPastDate
     Scenario: Verify the API Get Credits with Past EndDate
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?EndDate=2012-12-12
        Then response code should be 200
        And response body path should contain "endDate":"2012-12-12"
		And response body path should not contain "reference"
		And response body path should not contain "service"
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0
		
	@get-Credits_byInvalidPageSize
    Scenario: query all credits by InvalidPageSize
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?pageSize=Test123
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

	@get-Credits_byInvalidPageNumber
    Scenario: query all credits by InvalidPageNumber
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?pageNumber=Test123
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0
		
	@get-Credits_byInvalidSortDirection
    Scenario: query all credits by InvalidSortDirection
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?sortDirection=Test123
        Then response code should be 200
		
	@get-Credits_byInvalidSortField
    Scenario: query all credits by InvalidSortField
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?sortField=TEST
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : SortField must be a field in the result set
		
	@get-Credits_byInvalidName
    Scenario: query all credits by Invalidname
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?name=qwertsdfgsd
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

  @get-Credits_byInvalidAccountNum
  Scenario: query all charges by InvalidAccount
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    When I use HMAC and GET /credits?accountNumber=-1234
    Then response code should be 200
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "authCount":0
    And response body path should contain "authTotal":0
    And response body path should contain "saleCount":0
    And response body path should contain "saleTotal":0
    And response body path should contain "creditCount":0
    And response body path should contain "creditTotal":0
    And response body path should contain "totalCount":0
    And response body path should contain "totalVolume":0
		
	@get-Credits_byInvalidSource
    Scenario: query all charges by InvalidSource
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?source=Test123
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
		
	@get-Credits_byInvalidOrderNumber
    Scenario: query all credits by InvalidorderNumber
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?orderNumber=Test123
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0
		
	@get-Credits_byInvalidReference
    Scenario: query all credits by InvalidReference
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?reference=Test123
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0
			
	@get-Credits_byInvalidBatchRef
    Scenario: query all credits by InvalidBatchRef
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?batchReference=Test123
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0
			
	@get-Credits_byInvalidTotalAmount_WithLongAmount
    Scenario: query all credits by InvalidTotalAmount
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?totalAmount=213123123123123123
        Then response code should be 503
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.
		
	@get-Credits_byInvalidTotalAmount_byNegativeValue
    Scenario: query all credits by InvalidTotalAmount
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?totalAmount=-999
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

	@get-Credits_byInvalidTotalAmount_WithAlphaNumeric
    Scenario: query all credits by InvalidTotalAmount
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /credits?totalAmount=Test123
        Then response code should be 200
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

