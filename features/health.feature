@intg
Feature: API proxy Health Check
	As API administrator
    I want to monitor Apigee proxy and backend service health
    So I can alert when it is down
    
	@get-Ping
    Scenario: Verify the API proxy is responding
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /ping
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path environment should be qa
        And response body path apiproxy should be ach-v1
        And response body path client must with regex ^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$
      # And response body path latency must with regex ^\d{1,2}
        And response body path latency must with regex ^\d{1,2}
        And response body path message should be PONG

	@get-Status
    Scenario: Verify the backend service is responding
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /status
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path environment should be qa
    #   And response body path apiproxy should be `apiproxy`
        And response body path apiproxy should be ach-v1
        And response body path client must with regex ^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$
        And response body path latency must with regex ^\d{1,2}
        And response body path message should be STATUS
        
