@intg
Feature: Get Transactions
    As an API consumer
    I want to query transactions requests
    So that I know they have been processed

  @get-Transaction_getAllTransactions
  Scenario: query to get all transactions
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_bySaleTxReference
  Scenario: query Sale transaction with reference
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /charges?type=Sale
    Then response code should be 201
    And response body path status should be Approved
    When I use HMAC and GET /transactions?reference=
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body should contain the same reference number
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "type":"Sale"
    And response body path should not contain Bankcard
    And response body path should not contain Credit

  @get-Transaction_byCreditTxReference
  Scenario: query Credit transaction with reference
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And response body path status should be Approved
    When I use HMAC and GET /transactions?reference=
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body should contain the same reference number
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "type":"Credit"
    And response body path should not contain Bankcard
    And response body path should not contain Sale

  @get-Transaction_byStartDate
  Scenario: query by Start Date
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?startDate=08-13-2017
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "startDate":"2017-08-13"
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_byEndDate
  Scenario: query by End Date Transaction
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?endDate=10-12-2017
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "endDate":"2017-10-12"
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_byPageNo
  Scenario: query by page no
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?pageNumber=1
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "pageNumber":1
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_byPageSize
  Scenario: query by page size
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?pageSize=250
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "pageSize":250
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_bySortDirectionAsc
  Scenario: query Transaction by order
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?sortDirection=ascending
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_bySortDirectionDesc
  Scenario: query Transaction by order
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?sortDirection=descending
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_bySortField
  Scenario: query by Sort field
        Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?sortField=date
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_byisPurchaseCardTrue
  Scenario: query by purchase card True
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?isPurchaseCard=true
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "isPurchaseCard":true
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_byisPurchaseCardFalse
  Scenario: query by purchase card false
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?isPurchaseCard=false
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "isPurchaseCard":false
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_byName
  Scenario: query by Sort by name
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /credits
    And I use HMAC and GET /transactions?name=jean-luc
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "name":"jean-luc picard"
    And response body path should contain "paymentType":"ACH"
    And response body path should not contain Bankcard

  @get-Transaction_byAccountNumber
  Scenario: query by account Number
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?accountNumber=1234
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "type":"Sale"
    And response body path should contain "service":"ACH"
    And response body path should contain Credit
    And response body path should contain "accountNumber":"XXXXXXXXXX1234"

  @get-Transaction_byOrderNumber
  Scenario: query by Order Number
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And response body path status should be Approved
    And I use HMAC and GET /transactions?orderNumber=
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body should contain the same order number

  @get-Transaction_byReference
  Scenario: query by Reference Number
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And response body path status should be Approved
    And I use HMAC and GET /transactions?reference=
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body should contain the same reference number

  @get-Transaction_byTotalAmount
  Scenario: query by account Number
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I set body to { "transactionClass": "PPD", "amounts":{ "total": 11 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And response body path status should be Approved
    And I use HMAC and GET /transactions?totalAmount=11
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "total":11.0000


 # +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++

  @get-Transactions_byInvalidReference
  Scenario: query transactions with invalid reference
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I use HMAC and GET /transactions?reference=Test123
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should not contain "reference"
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "authCount":0
    And response body path should contain "authTotal":0
    And response body path should contain "saleCount":0
    And response body path should contain "saleTotal":0
    And response body path should contain "creditCount":0
    And response body path should contain "creditTotal":0
    And response body path should contain "totalCount":0
    And response body path should contain "totalVolume":0

  @get-Transactions_byInvalidName
  Scenario: query transactions with invalid Name
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    And I use HMAC and GET /transactions?name=Test123
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should not contain "name"
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "authCount":0
    And response body path should contain "authTotal":0
    And response body path should contain "saleCount":0
    And response body path should contain "saleTotal":0
    And response body path should contain "creditCount":0
    And response body path should contain "creditTotal":0
    And response body path should contain "totalCount":0
    And response body path should contain "totalVolume":0
		
	@get-Transactions_StartDate_AsFutureDate
     Scenario: Verify the API Get Transactions with Future StartDate
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /transactions?StartDate=2030-12-12
        Then response code should be 200
        And response body path should contain "startDate":"2030-12-12"
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

  @get-Transactions_EndDate_AsPastDate
     Scenario: Verify the API Get Transactions with Past EndDate
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /transactions?EndDate=2012-12-12
        Then response code should be 200
        And response body path should contain "endDate":"2012-12-12"
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

  @get-Transactions_byinValidAccountNumber
  Scenario: Verify the API Get transactions with inValid AccountNumber
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?accountNumber=Test123
    Then response code should be 200
    And response body path should not contain "AccountNumber"
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "authCount":0
    And response body path should contain "authTotal":0
    And response body path should contain "saleCount":0
    And response body path should contain "saleTotal":0
    And response body path should contain "creditCount":0
    And response body path should contain "creditTotal":0
    And response body path should contain "totalCount":0
    And response body path should contain "totalVolume":0

	@get-Transactions_byinValidOrderNumber
     Scenario: Verify the API Get transactions with inValidOrderNumber
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /transactions?orderNumber=Test123
        Then response code should be 200
		And response body path should not contain "orderNumber"
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

#      Test case failing - drop down provided instead of text field
	
#	@get-Transactions_byinValidSource
#     Scenario: Verify the API Get transactions with inValidSource
#       Given I set clientId header to `clientId`
#      And I set merchantId header to `merchantId`
#        And I set merchantKey header to `merchantKey`
#        And I set content-type header to application/json
#		When I use HMAC and GET /transactions?Source=Test123
#        Then response code should be 200
#        And response body path should not contain "source"
#        And response body path should contain "paymentType":"ACH"
#        And response body path should contain "authCount":0
#        And response body path should contain "authTotal":0
#        And response body path should contain "saleCount":0
#        And response body path should contain "saleTotal":0
#        And response body path should contain "creditCount":0
#        And response body path should contain "creditTotal":0
#        And response body path should contain "totalCount":0
#        And response body path should contain "totalVolume":0
	
	@get-Transactions_byinValidIsPurchaseCard
     Scenario: Verify the API Get transactions with inValidPurchaseCard
        Given I set clientId header to clientId
        And I set merchantId header to merchantId
        And I set merchantKey header to merchantKey
        And I set content-type header to application/json
        And I set nonce header to nonce
        And I set timestamp header to timestamp
        And I set Authorization header to Authorization
		When I use HMAC and GET /transactions?IsPurchaseCard=Test123
        Then response code should be 200
        And response body path should not contain "IsPurchaseCard"
        And response body path should contain "paymentType":"ACH"
        And response body path should contain "authCount":0
        And response body path should contain "authTotal":0
        And response body path should contain "saleCount":0
        And response body path should contain "saleTotal":0
        And response body path should contain "creditCount":0
        And response body path should contain "creditTotal":0
        And response body path should contain "totalCount":0
        And response body path should contain "totalVolume":0

  @get-Transactions_byinValidBatchReference
  Scenario: Verify the API Get transactions with inValidBatchReference
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?batchReference=Test123
    Then response code should be 200
    And response body path should not contain "batchReference"
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "authCount":0
    And response body path should contain "authTotal":0
    And response body path should contain "saleCount":0
    And response body path should contain "saleTotal":0
    And response body path should contain "creditCount":0
    And response body path should contain "creditTotal":0
    And response body path should contain "totalCount":0
    And response body path should contain "totalVolume":0

  @get-Transactions_byinValidTotalAmount
  Scenario: Verify the API Get transactions with inValidTotalAmount
    Given I set clientId header to clientId
    And I set merchantId header to merchantId
    And I set merchantKey header to merchantKey
    And I set content-type header to application/json
    And I set nonce header to nonce
    And I set timestamp header to timestamp
    And I set Authorization header to Authorization
    When I use HMAC and GET /transactions?totalAmount=Test123
    Then response code should be 200
    And response body path should not contain "source"
    And response body path should contain "paymentType":"ACH"
    And response body path should contain "authCount":0
    And response body path should contain "authTotal":0
    And response body path should contain "saleCount":0
    And response body path should contain "saleTotal":0
    And response body path should contain "creditCount":0
    And response body path should contain "creditTotal":0
    And response body path should contain "totalCount":0
    And response body path should contain "totalVolume":0
		
		
		
		
		
	